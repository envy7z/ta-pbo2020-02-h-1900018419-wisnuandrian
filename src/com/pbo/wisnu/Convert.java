package com.pbo.wisnu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.InputMismatchException;

public class Convert extends JFrame {
    JButton calculate;
    JLabel label , label2;
    JTextField textfield , textfield2;
    JRadioButton working, resident, nonResident;

    // call super to set title and set layout
    public Convert() {
        super("Personal Tax");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setVisible(true);
        setSize(430, 150);
        setLocation(500, 300);

        //Creates calculate button
        calculate = new JButton("Calculate");
        calculate.setBounds(220, 90, 210, 40);
        calculate.setOpaque(true);

        calculate.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                calculate.setBackground(Color.blue);
                calculate.setForeground(Color.blue);

            }

            @Override
            public void mouseExited(java.awt.event.MouseEvent evt) {

                calculate.setBackground(UIManager.getColor("control"));
                calculate.setForeground(Color.BLACK);
            }
        });


        //Income label
        label = new JLabel();
        label.setText("Total Income in $ :");
        label.setBounds(10, 10, 150, 15);

        label2 = new JLabel();
        label2.setText("Convert to idr ->");
        label2.setBounds(10,55,100,15);

        //Income textfield
        textfield = new JTextField();
        textfield.setBounds(220, 0, 210, 40);

        //Income textfield
        textfield2 = new JTextField();
        textfield2.setBounds(220, 45, 210, 40);

        add(textfield2);
        add(textfield);
        add(label);
        add(label2);
        add(calculate);

        calculate.addActionListener((ActionEvent arg0) -> {
            String getIncome = textfield.getText();
            Double hasil ;

            hasil = Double.parseDouble(getIncome) * 10889;

            textfield2.setText(hasil.toString());
        });

    }

}

package com.pbo.wisnu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

class BusinessPersonal extends JFrame {

    JButton b1, b2 , b3;

    public BusinessPersonal() {
        super("Tax Calculator");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setVisible(true);
        setSize(760, 100);
        setLocation(250, 300);

        //creates buttons
        b1 = new JButton("Business Tax Calculator");
        b2 = new JButton("Personal Tax Calculator");
        b3 = new JButton("Convert IDR To $  ");

        b1.setOpaque(true);
        b2.setOpaque(true);
        b3.setOpaque(true);

        //Changes colour on mouse hover for Button 1
        b1.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                b1.setBackground(Color.BLUE);
                b1.setBorderPainted(false);
                b1.setForeground(Color.white);
            }
            //Changes colour back on Button 1 when hover exited

            @Override
            public void mouseExited(java.awt.event.MouseEvent evt) {

                b1.setBackground(UIManager.getColor("control"));
                b1.setBorderPainted(true);
                b1.setForeground(Color.BLACK);
            }
        });

        //Changes colour on mouse hover for Button 2
        b2.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                b2.setBackground(Color.blue);
                b2.setBorderPainted(false);
                b2.setForeground(Color.white);

            }

            //Changes colour back on Button 2 when hover exited
            @Override
            public void mouseExited(java.awt.event.MouseEvent evt) {

                b2.setBackground(UIManager.getColor("control"));
                b2.setBorderPainted(true);
                b2.setForeground(Color.BLACK);
            }
        });

        //Changes colour on mouse hover for Button 2
        b3.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                b3.setBackground(Color.blue);
                b3.setBorderPainted(false);
                b3.setForeground(Color.white);

            }

            //Changes colour back on Button 2 when hover exited
            @Override
            public void mouseExited(java.awt.event.MouseEvent evt) {

                b3.setBackground(UIManager.getColor("control"));
                b3.setBorderPainted(true);
                b3.setForeground(Color.BLACK);
            }
        });

        //Button 2 action listener
        b2.addActionListener((ActionEvent e) -> {

            new PersonalCalculator();
            dispose();
        });

        //Button 1 action listener
        b1.addActionListener((ActionEvent e) -> {
            new BusinessCalculator();
            dispose();
        });

        //Button 3
        b3.addActionListener((ActionEvent e) -> {
            new Convert();
            dispose();
        });

        //sets button sizes
        b1.setBounds(0, 0, 250, 80);
        b2.setBounds(260, 0, 250, 80);
        b3.setBounds(520, 0, 250, 80);


        //Add buttons to frame
        add(b1);
        add(b2);
        add(b3);
    }
}

package com.pbo.wisnu;

public interface TaxProfile {
    public double getPayableTax();
    public String getTaxID();
    public String getNameOfTaxPayer();
    
}
